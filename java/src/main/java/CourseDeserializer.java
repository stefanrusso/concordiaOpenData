// https://www.baeldung.com/jackson-deserialization
// https://dzone.com/articles/custom-json-deserialization-with-jackson

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class CourseDeserializer extends JsonDeserializer<Course> {

    @Override
    public Course deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);

        String ID = node.get("ID").asText();
        String title = node.get("title").asText().trim();
        String subject = node.get("subject").asText().trim();
        String catalog = node.get("catalog").asText().trim();
        String career = node.get("career").asText().trim();
        String classUnit = node.get("classUnit").asText().trim();
        // To be replaced by prerequisite parser function
        StringBuilder prerequisites = new StringBuilder(node.get("prerequisites").asText().trim());
        StringBuilder corequisites = new StringBuilder("");
        StringBuilder antirequisites = new StringBuilder("");
        StringBuilder otherrequisites = new StringBuilder("");

        // TODO: How to handle null values in object mapper?
//         String crossListed = node.get("crossListed").asText();
        String crossListed = "";


        return new Course(ID, title, subject, catalog, career, classUnit, prerequisites, corequisites, antirequisites, otherrequisites, crossListed);
    }
}
