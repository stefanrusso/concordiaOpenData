public enum RequisiteType {
    PREREQUISITE,
    COREQUISITE,
    ANTIREQUISITE,
    OTHER
}
