import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.github.cdimascio.dotenv.Dotenv;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {

		// Finds the .env file and loads the vars
		Dotenv dotenv = Dotenv.load();

		String apiURL = "https://opendata.concordia.ca/API/v1/course/catalog/filter/COMP/*/UGRD";
		final String apiId = dotenv.get("API_ID");
		final String apiKey = dotenv.get("API_KEY");

		// Creates the Authenticator object
		// https://labs.consol.de/development/2017/03/14/getting-started-with-java9-httpclient.html
		Authenticator authenticator = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(apiId, apiKey.toCharArray());
			}
		};

		// Create the HTTP Client
		HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).authenticator(authenticator)
				.build();

		// Form the request
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(apiURL)).GET().build();

		// Send the request and get the response
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

		// Create a list of courses and map the returned values to the Course class
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Course.class, new CourseDeserializer());

		mapper.registerModule(module);

		ArrayList<Course> courseListWithDupes = mapper.readValue(response.body(),
				new TypeReference<ArrayList<Course>>() {
				});

//		ArrayList<Course> courseList = new ArrayList<Course>();

//		for (Course course : courseListWithDupes) {
//			if (!courseList.contains(course)) {
//				System.out.println(course.toString());
//				courseList.add(course);
//			}
//		}
//
//		Collections.sort(courseList);
		
//		Course[] courseArray = courseList.toArray();

		// TODO Sort ArrayList
		// TODO Loop through ArrayList and delete duplicates
		// Key/Value -> Map -> Type of Map -> Graphs (Directed or DAG)

        HashMap<String, Course> courseList = new HashMap<String, Course>();

        for (Course course : courseListWithDupes) {
//            System.out.println(course.toFormattedString());
            courseList.put(course.getID(), course);
        }

        for(Entry<String, Course> course : courseList.entrySet()) {
            System.out.println(course.getValue().toFormattedString());
//			System.out.println(course.getValue().toString());
        }
	}

}
