

import java.util.Objects;

public class Course implements Comparable<Course> {

    private String ID;
    private String title;
    private String subject;
    private String catalog;
    private String career;
    private String classUnit;
    private StringBuilder prerequisites;
    private StringBuilder corequisites;
    private StringBuilder antirequisites;
    private StringBuilder otherrequisites;
    private String crosslisted;

    // Empty constructor required by Jackson ObjectMapper
    public Course() {
    }

    public Course(String ID, String title, String subject, String catalog, String career, String classUnit,
                  StringBuilder prerequisites, StringBuilder corequisites, StringBuilder antirequisites,
                  StringBuilder otherrequisites, String crosslisted) {
        this.ID = ID;
        this.title = title;
        this.subject = subject;
        this.catalog = catalog;
        this.career = career;
        this.classUnit = classUnit;
        this.prerequisites = prerequisites;
        this.corequisites = corequisites;
        this.antirequisites = antirequisites;
        this.otherrequisites = otherrequisites;
        this.crosslisted = crosslisted;
    }

    public String toString() {
        return "\nID: " + this.ID
                + "\nTitle: " + this.title
                + "\nSubject: " + this.subject
                + "\nCatalog: " + this.catalog
                + "\nCareer: " + this.career
                + "\nClassUnit: " + this.classUnit
                + "\nPrerequisites: " + this.prerequisites
                + "\nCo-requisites: " + this.corequisites
                + "\nAntirequisites: " + this.antirequisites
                + "\nOther Requisites: " + this.otherrequisites
                + "\nCrossListed: " + this.crosslisted;
    }

    public String toFormattedString() {
        return this.subject + this.catalog + " - " + this.prerequisites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(getID(), course.getID()) &&
                Objects.equals(getTitle(), course.getTitle()) &&
                Objects.equals(getSubject(), course.getSubject()) &&
                Objects.equals(getCatalog(), course.getCatalog()) &&
                Objects.equals(getCareer(), course.getCareer()) &&
                Objects.equals(getClassUnit(), course.getClassUnit()) &&
                Objects.equals(getPrerequisites(), course.getPrerequisites()) &&
                Objects.equals(getCorequisites(), course.getCorequisites()) &&
                Objects.equals(getAntirequisites(), course.getAntirequisites()) &&
                Objects.equals(getOtherrequisites(), course.getOtherrequisites()) &&
                Objects.equals(getCrosslisted(), course.getCrosslisted());
    }

    public int compareTo(Course otherCourse) {
        // Compare two courses with different subjects
        if (this.subject.compareTo(otherCourse.subject) < 0) {
            return -1;
        } else if (this.subject.compareTo(otherCourse.subject) > 0) {
            return 1;
        } else {
            // Compare two courses with same subject and different catalog #
            if (this.catalog.compareTo(otherCourse.catalog) < 0) {
                return -1;
            } else if (this.catalog.compareTo(otherCourse.catalog) > 0) {
                return 1;
            } else { // Compare two courses with same subject and same catalog
                return 0;
            }
        }
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getClassUnit() {
        return classUnit;
    }

    public void setClassUnit(String classUnit) {
        this.classUnit = classUnit;
    }

    public StringBuilder getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(StringBuilder prerequisites) {
        this.prerequisites = prerequisites;
    }

    public StringBuilder getCorequisites() {
        return corequisites;
    }

    public void setCorequisites(StringBuilder corequisites) {
        this.corequisites = corequisites;
    }

    public StringBuilder getAntirequisites() {
        return antirequisites;
    }

    public void setAntirequisites(StringBuilder antirequisites) {
        this.antirequisites = antirequisites;
    }

    public StringBuilder getOtherrequisites() {
        return otherrequisites;
    }

    public void setOtherrequisites(StringBuilder otherrequisites) {
        this.otherrequisites = otherrequisites;
    }

    public String getCrosslisted() {
        return crosslisted;
    }

    public void setCrosslisted(String crosslisted) {
        this.crosslisted = crosslisted;
    }
}
