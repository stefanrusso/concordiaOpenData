#![deny(warnings)] // Issue errors instead of warnings

extern crate dotenv;
#[macro_use]
extern crate dotenv_codegen;

use serde_json;
use serde::{Serialize, Deserialize};
use std::fmt;

#[derive(Serialize, Deserialize)]
struct Course {
    #[serde(rename = "ID")]  // Difference in API data names vs rust style preferences
    id: String,
    title: String,
    subject: String,
    catalog: String,
    career: String,
    #[serde(rename = "classUnit")] // Difference in API data names vs rust style preferences
    class_unit: String,
    prerequisites: String,
    crosslisted: Option<String>,  // Option<String> used due to null values in API data
}

// equivalent of implementing the toString() interface in Java for a class
impl fmt::Display for Course {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{},{},\"{}\",{}", self.subject, self.catalog, self.title, self.class_unit)
    }
}

#[tokio::main]  // Start the program with an async call to main() instead of a synchronous call
async fn main() -> Result<(), reqwest::Error> {
    let api_id = dotenv!("API_ID");
    let api_key = dotenv!("API_KEY");

    let request_url = "https://opendata.concordia.ca/API/v1/course/catalog/filter/*/*/UGRD";

    // Build the GET request
    let response = reqwest::Client::new()
        .get(request_url)
        .basic_auth(api_id.clone(), Some(api_key.clone()))
        .send()
        .await?;

    println!("Status: {}", response.status());

    // Get the body of the response
    let body = response.text().await?;

    // Deserialize the response into json, then into a Vector
    let _courses: Vec<Course> = serde_json::from_str(&body).unwrap();
    
    // Print things
    for course in _courses {
        println!("{}", course);
    }
    Ok(())
}
